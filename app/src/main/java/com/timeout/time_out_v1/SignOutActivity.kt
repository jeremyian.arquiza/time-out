package com.timeout.time_out_v1

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.timeout.time_out_v1.Receivers.BlockingReceiver
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SignOutActivity : AppCompatActivity() {

    private lateinit var fireAuth: FirebaseAuth
    private lateinit var reAuthEmail: EditText
    private lateinit var reAuthPassword: EditText
    private lateinit var credential: AuthCredential
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var outChildBtn : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_out)

        fireAuth = FirebaseAuth.getInstance()
        firebaseUser = fireAuth.currentUser!!

        reAuthEmail = findViewById(R.id.signoutchild_email_et)
        reAuthPassword = findViewById(R.id.signoutchild_pin_et)
        outChildBtn = findViewById(R.id.signoutchild_button)

        outChildBtn.setOnClickListener{
            signOutChild()
        }
    }

    private fun signOutChild(){
        val emailValue = reAuthEmail.text.toString()
        val passValue = reAuthPassword.text.toString()

        credential = EmailAuthProvider.getCredential(emailValue, passValue)
        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager

        firebaseUser.reauthenticate(credential).addOnCompleteListener {
                task ->
            if (task.isSuccessful)
            {
                fireAuth.signOut()
                val startIntent = Intent(this@SignOutActivity, BlockingReceiver::class.java)
                val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                alarmManager.cancel(startPendingIntent)
                val intent = Intent(this@SignOutActivity, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                Toast.makeText(this@SignOutActivity, "Child Profile successfully signed out.", Toast.LENGTH_SHORT).show()
            }
            else
            {
                Toast.makeText(this@SignOutActivity, "Invalid credentials entered.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}