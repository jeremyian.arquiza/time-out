package com.timeout.time_out_v1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var passwordIB: ImageButton
    private lateinit var passwordET: EditText
    private lateinit var fireAuth: FirebaseAuth
    private lateinit var secretBtn: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        passwordIB = findViewById(R.id.show_login_password_button)
        passwordET = findViewById(R.id.password_edittext_login)

        passwordIB.setOnTouchListener { v: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP) {
                passwordET.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
            }
            if (event.action == MotionEvent.ACTION_DOWN) {
                passwordET.setInputType(InputType.TYPE_CLASS_TEXT)
            }
            true
        }

        fireAuth = FirebaseAuth.getInstance()
        if(fireAuth.currentUser != null)
        {
            val intent2 = Intent(this@MainActivity, HomeActivity::class.java)
            startActivity(intent2)
            finish()
        }

        val actionBar = supportActionBar

        actionBar!!.title = "Login"

        val login_button_login: Button = findViewById<Button>(R.id.login_button_login)
        secretBtn = findViewById(R.id.main_timeout_logo_iv)

        secretBtn.setOnClickListener{
            val intent = Intent(this@MainActivity, DeleteActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }


        login_button_login.setOnClickListener {
            val email = findViewById<EditText>(R.id.email_edittext_login).text.toString()
            val password = findViewById<EditText>(R.id.password_edittext_login).text.toString()

            when {
                TextUtils.isEmpty(email.trim {it <= ' '}) -> {
                    Toast.makeText(
                        this@MainActivity,
                        "Please enter email.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                TextUtils.isEmpty(password.trim {it <= ' '}) -> {
                    Toast.makeText(
                        this@MainActivity,
                        "Please enter password.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                else -> {
                    val Remail: String = email.trim {it <= ' '}
                    val Rpassword: String = password.trim {it <= ' '}


                    fireAuth.signInWithEmailAndPassword(Remail, Rpassword)
                        .addOnCompleteListener { task ->

                            if (task.isSuccessful) {

                                Toast.makeText(
                                    this@MainActivity,
                                    "You are logged in successfully.",
                                    Toast.LENGTH_SHORT
                                ).show()

                                val intent = Intent(this@MainActivity, HomeActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                intent.putExtra("user_id", FirebaseAuth.getInstance().currentUser!!.uid)
                                intent.putExtra("email_id", Remail)
                                startActivity(intent)
                                finish()
                            }
                            else
                            {
                                Toast.makeText(
                                    this@MainActivity,
                                    task.exception!!.message.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                }
            }
        }
    }
}