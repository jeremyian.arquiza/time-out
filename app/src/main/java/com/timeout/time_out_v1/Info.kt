package com.timeout.time_out_v1

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

class Info : AppCompatActivity() {

    private lateinit var signOutBtn : Button
    private lateinit var fireAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        fireAuth = FirebaseAuth.getInstance()

        var count = 0

        signOutBtn = findViewById(R.id.signout_button)
        signOutBtn.setBackgroundColor(Color.TRANSPARENT)
        signOutBtn.setTextColor(Color.TRANSPARENT)

        signOutBtn.setOnClickListener{
            count = count + 1
            if(count == 10)
            {


                BookmarksHolder.resetChosenValue()
                BookmarksHolder.resetChosenKey()
                BookmarksHolder.resetKeys()
                BookmarksHolder.resetValues()
                val sampleDialog = ExitDialog()
                sampleDialog.show(supportFragmentManager, "")
                /*fireAuth.signOut()
                val intent = Intent(this@Info, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                 */
                count = 0
            }
        }
    }
}