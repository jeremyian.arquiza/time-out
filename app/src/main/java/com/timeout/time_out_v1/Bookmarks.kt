package com.timeout.time_out_v1

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class Bookmarks : AppCompatActivity() {

    private var pBookmarkKeys = arrayListOf<String>()
    private var pBookmarkValues = arrayListOf<String>()
    private lateinit var fireAuth : FirebaseAuth
    lateinit var currentUserID : String
    private lateinit var db : DatabaseReference
    private lateinit var db2 : DatabaseReference
    private lateinit var openBookmarkBtn : Button
    private lateinit var removeBookmarkBtn : Button
    private lateinit var lv : ListView
    var bookmarkList = arrayListOf<String>()
    lateinit var arrayAdapter : ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmarks)

        fireAuth = FirebaseAuth.getInstance()
        currentUserID = fireAuth.currentUser?.uid.toString()

        lv = findViewById(R.id.bookmarks_lv)
        removeBookmarkBtn = findViewById(R.id.delBookmark_button)
        openBookmarkBtn = findViewById(R.id.openBookmark_button)

        lv.setOnItemClickListener{ adapterView: AdapterView<*>?, view: View?, position: Int, l: Long ->
            BookmarksHolder.resetChosenKey()
            BookmarksHolder.resetChosenValue()
            BookmarksHolder.chosenKey = BookmarksHolder.bookmarkKeys[position]
            BookmarksHolder.chosenValue = BookmarksHolder.bookmarkValues[position]
            Toast.makeText(this@Bookmarks, "Chosen Bookmark was: " + BookmarksHolder.chosenValue, Toast.LENGTH_SHORT).show()

            for (i in 0 until lv.getChildCount()) {
                if (position === i) {
                    lv.getChildAt(i).setBackgroundColor(Color.CYAN)
                } else {
                    lv.getChildAt(i).setBackgroundColor(Color.WHITE)
                }
            }
        }

        BookmarksHolder.bookmarkKeys = pBookmarkKeys
        BookmarksHolder.bookmarkValues = pBookmarkValues

        arrayAdapter = ArrayAdapter(
            this@Bookmarks, R.layout.textview_layout, bookmarkList
        )
        getBookmarks()

        openBookmarkBtn.setOnClickListener{
            openBookmark()
        }

        removeBookmarkBtn.setOnClickListener{
            deleteBookmark()
        }
    }

    fun getBookmarks()
    {
        db = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("Bookmarks")
        db.keepSynced(false)
        db.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    for (getSnapshot in snapshot.children)
                    {
                        val website = getSnapshot.value
                        val webkey = getSnapshot.key
                        BookmarksHolder.bookmarkKeys.add(webkey)
                        BookmarksHolder.bookmarkValues.add(website.toString())
                        bookmarkList.add(website!!.toString())
                    }
                    lv.adapter = arrayAdapter
                }
                else
                {
                    Toast.makeText(this@Bookmarks, "No Bookmarks yet", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("Bookmarks", "Bookmarks Error")
            }
        })
    }

    fun deleteBookmark()
    {
        if(BookmarksHolder.chosenKey == null)
        {
            Toast.makeText(this@Bookmarks, "No Bookmark Chosen Yet", Toast.LENGTH_SHORT).show()
        }
        else
        {
            val delBookmark = BookmarksHolder.chosenKey
            db2 = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("Bookmarks").child(delBookmark)
            db2.removeValue().addOnCompleteListener{task ->
                if(task.isSuccessful)
                {
                    Toast.makeText(this@Bookmarks, "Bookmark was successfully deleted", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this@Bookmarks, Bookmarks::class.java)
                    startActivity(intent)
                }
                else
                {
                    Toast.makeText(this@Bookmarks, "There was a problem in deleting the bookmark.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun openBookmark()
    {
        if(BookmarksHolder.chosenValue == null)
        {
            Toast.makeText(this@Bookmarks, "No Bookmark Chosen Yet", Toast.LENGTH_SHORT).show()
        }
        else
        {
            val bookmarkGo = BookmarksHolder.chosenValue.toString()
            val intent = Intent(this@Bookmarks, HomeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("bookmark", bookmarkGo)
            startActivity(intent)
        }
    }
}