package com.timeout.time_out_v1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class DeleteActivity : AppCompatActivity() {

    private lateinit var fireAuth: FirebaseAuth
    private lateinit var reAuthEmail: EditText
    private lateinit var reAuthPassword: EditText
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var delChildBtn : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete)

        fireAuth = FirebaseAuth.getInstance()

        reAuthEmail = findViewById(R.id.delchild_email_et)
        reAuthPassword = findViewById(R.id.delchild_pin_et)
        delChildBtn = findViewById(R.id.delchild_button)


        delChildBtn.setOnClickListener{
            deleteChild()
        }
    }

    private fun deleteChild(){
        val emailValue = reAuthEmail.text.toString()
        val passValue = reAuthPassword.text.toString()


        fireAuth.signInWithEmailAndPassword(emailValue,passValue).addOnCompleteListener {
            task ->
            if (task.isSuccessful)
            {
                val intent = Intent(this@DeleteActivity, SuccessActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            }
            else
            {
                Toast.makeText(this@DeleteActivity, "Invalid Credentials entered.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}