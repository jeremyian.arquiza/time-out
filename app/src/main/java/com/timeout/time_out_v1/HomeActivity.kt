package com.timeout.time_out_v1

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.ResolveInfo
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.*
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import java.util.*
import android.app.DownloadManager

import android.os.Environment
import android.webkit.*


class HomeActivity : AppCompatActivity() {

    lateinit var startTime : String
    lateinit var stopTime : String
    lateinit var currentUserID : String
    private lateinit var webView: WebView
    private val URL = "https://www.google.com"
    private lateinit var myUrlBar: EditText
    private lateinit var myButton: ImageButton
    var packlist = arrayListOf<String>()
    var history = arrayListOf<String>()
    var appNames = arrayListOf<String>()
    lateinit var applist : List<ResolveInfo>
    private lateinit var historyList : WebBackForwardList
    private lateinit var histitem : WebHistoryItem
    private lateinit var db : DatabaseReference
    private lateinit var db2 : DatabaseReference
    private lateinit var db3 : DatabaseReference
    private lateinit var db4 : DatabaseReference
    private lateinit var db5 : DatabaseReference
    private lateinit var calendar : Calendar
    val proto = "https://"
    var currentSize = 0
    private lateinit var fireAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        fireAuth = FirebaseAuth.getInstance()
        currentUserID = fireAuth.currentUser?.uid.toString()

        var calendar: Calendar

        //Settings for Title Bar
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setLogo(R.drawable.timeouticon)
        supportActionBar?.setDisplayUseLogoEnabled(true)

        //Variable declarations for UI interactibles
        webView = findViewById(R.id.myWebView)
        myUrlBar = findViewById(R.id.myUrlBar)
        myButton = findViewById(R.id.GoButton)


        //Enabling "Clear" button on URL Bar
        myUrlBar.onRightDrawableClicked {
            it.text.clear()
        }
        myUrlBar.makeClearableEditText(null, null)

        //Setting initial value of URL Bar to current website URL
        myUrlBar.setText(webView.url)



        //Collect list of websites to block
        db = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("Sites")
        db.keepSynced(true)
        db.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    packlist.clear()
                    for (getSnapshot in snapshot.children)
                    {
                        val website = getSnapshot.value
                        packlist.add(website!!.toString())
                    }
                }
                else
                {
                    Toast.makeText(this@HomeActivity, "There was a problem with loading the websites, kindly contact your parents.",Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("BlockedSites", "Blocked Websites Error")
            }
        })

        //Website navigation function for blocking manually entered URL's
        myButton.setOnClickListener {
            val url: String? = webView.url
            var site: String? = myUrlBar.text.toString()
            for (pack in packlist) {
                if (site != null) {
                    if (url!!.contains(pack)) {
                        if (webView.canGoBack()) {
                            webView.goBack()
                            Toast.makeText(this@HomeActivity, "BLOCKED", Toast.LENGTH_SHORT).show()
                        } else {
                            webView.loadUrl(URL)
                            Toast.makeText(this@HomeActivity, "BLOCKED", Toast.LENGTH_SHORT).show()
                        }
                        Toast.makeText(this@HomeActivity, "BLOCKED", Toast.LENGTH_SHORT).show()
                    }
                    else if (url.contains("porn") || url.contains("xxx"))
                    {
                        webView.loadUrl(URL)
                        Toast.makeText(this@HomeActivity, "BLOCKED", Toast.LENGTH_SHORT).show()
                    }
                    else
                    {
                        webView.loadUrl(proto + site)
                        myUrlBar.setText(webView.url)
                    }
                }
            }
        }

        //Automatic blocking of websites when clicking on URL's
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                for (pack in packlist) {
                    if (url != null) {
                        if (url.contains(pack) || url.contains("porn") || url.contains("xxx")) {
                            webView.loadUrl(URL)
                            Toast.makeText(this@HomeActivity, "BLOCKED", Toast.LENGTH_SHORT).show()
                            return true
                        }
                    }
                }
                return false
            }

            //Upload Browser History when History entries reach a total amount of 8 (eight)
            override fun onPageFinished(view: WebView?, url: String?) {
                myUrlBar.setText(webView.url)
                if (webView.copyBackForwardList().size % 4 == 0)
                {
                    historyList = webView.copyBackForwardList()
                    currentSize = historyList.size
                    for (i in 0 until currentSize) {
                        histitem = historyList.getItemAtIndex(i)
                        val websites = histitem.url
                        history.add(websites!!.toString())
                    }
                    db4 = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("History")
                    db4.setValue(history).addOnCompleteListener{ task ->
                        if(task.isSuccessful)
                        {
                            Log.d("History Upload", "Complete")
                        }
                        else
                        {
                            Log.d("History Upload", "Error")
                        }
                    }
                    history.clear()
                }
            }
        }

        //Allows In-App browser to download files
        webView.setDownloadListener(DownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(
                Uri.parse(url)
            )

            val filename = URLUtil.guessFileName(url, contentDisposition, mimetype)
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                filename
            )
            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            dm.enqueue(request)
            Toast.makeText(
                applicationContext,
                "Downloading File",  //To notify the Client that the file is being downloaded
                Toast.LENGTH_LONG
            ).show()
        })


        //Initializing AlarmManager variable to be used by all following functions
        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager

        //Enabling Javascript for Web Browser
        webView.apply {
            //If the user comes from the Bookmarks Page, the selected bookmark will be loaded
            //Else, the browser will load the default URL (Google)
            val receivedBookmark = intent.getSerializableExtra("bookmark")
            if(receivedBookmark == null)
            {
                loadUrl(URL)
            }
            else
            {
                loadUrl(receivedBookmark.toString())
            }
            settings.javaScriptEnabled = true
        }

        //Function to automatically change value of URL Bar based on current website
        myUrlBar.setOnFocusChangeListener { view, b ->
            if (b)
            {
                myUrlBar.setText(webView.url)
            }
        }


        //Get AM Start Blocking Time
        db2 = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID)
        db2.keepSynced(true)
        db2.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                startTime = snapshot.child("startTime").value.toString()
                stopTime = snapshot.child("stopTime").value.toString()
                //Toast.makeText(this@HomeActivity, "START TIME IS: $startTime",Toast.LENGTH_SHORT).show()
                //Toast.makeText(this@HomeActivity, "STOP TIME IS: $stopTime",Toast.LENGTH_SHORT).show()

                //If the value is set to 0, the Blocking is stopped
                if(startTime.equals("0"))
                {
                    val startIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.BlockingReceiver::class.java)
                    val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                    alarmManager.cancel(startPendingIntent)
                }

                //If the value is set to 1, the Blocking will follow the default blocking time of
                //8 AM until 4 PM
                else if(startTime.equals("1"))
                {
                    calendar = Calendar.getInstance()
                    calendar[Calendar.HOUR_OF_DAY] = 8 //8 AM Blocking Start
                    calendar[Calendar.MINUTE] = 0
                    calendar[Calendar.SECOND] = 0
                    if(System.currentTimeMillis() > calendar.timeInMillis)
                    {
                        val startIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.BlockingReceiver::class.java)
                        startIntent.putExtra("stop", stopTime)
                        startIntent.putExtra("pack", "0")
                        val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), startPendingIntent)
                    }
                    else
                    {
                        val startIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.BlockingReceiver::class.java)
                        startIntent.putExtra("stop", stopTime)
                        startIntent.putExtra("pack", "0")
                        val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, startPendingIntent)
                    }
                }
                //If the value is later than the current time, the blocking will start
                //immediately
                else
                {
                    if(System.currentTimeMillis() > startTime.toLong())
                    {
                        val startIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.BlockingReceiver::class.java)
                        startIntent.putExtra("stop", stopTime)
                        startIntent.putExtra("pack", "0")
                        val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), startPendingIntent)
                    }
                    else
                    {
                        val startIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.BlockingReceiver::class.java)
                        startIntent.putExtra("stop", stopTime)
                        startIntent.putExtra("pack", "0")
                        val startPendingIntent = PendingIntent.getBroadcast(applicationContext, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                        alarmManager.set(AlarmManager.RTC_WAKEUP, startTime.toLong(), startPendingIntent)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("Times", "Start and Stop Time Error")
            }
        })


        //Set App Usage Collection
        val usageIntent = Intent(this@HomeActivity, com.timeout.time_out_v1.Receivers.UsageReceiver::class.java)
        val usagePendingIntent = PendingIntent.getBroadcast(applicationContext, 69, usageIntent, 0)
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), usagePendingIntent)

        uploadInstalledApps()
    }

    //Function to open more options menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.custom_menu, menu)
        return true
    }

    //Other Browser functions
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId)
        {
            R.id.Home ->
            {
                webView.loadUrl(URL)
            }
            R.id.Bookmark ->
            {
                bookmarkSite()
            }
            R.id.Refresh ->
            {
                webView.reload()
            }
            R.id.ShowBookmarks ->
            {
                val intent = Intent(this@HomeActivity, Bookmarks::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
            }
            R.id.ShowHistory ->
            {
                getHistory()
                val intent = Intent(this@HomeActivity, History::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("key", history)
                startActivity(intent)
            }
            R.id.Information ->
            {
                val intent = Intent(this@HomeActivity, Info::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
            }
        }
        return true
    }

    //Function to allow user to go back on previous sites instead of exiting the application
    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        }
        else
        {
            super.onBackPressed()
        }
    }

    //Function to collect the Webview Browser History
    fun getHistory()
    {
        historyList = webView.copyBackForwardList()
        currentSize = historyList.size
        for (i in 0 until currentSize) {
            histitem = historyList.getItemAtIndex(i)
            val websites = histitem.url
            history.add(websites!!.toString())
        }
    }

    //Function to enable the Clear button on the URL Bar
    private fun addRightCancelDrawable(editText: EditText) {
        val cancel = ContextCompat.getDrawable(this, R.drawable.ic_cancel_black)
        cancel?.setBounds(0,0, cancel.intrinsicWidth, cancel.intrinsicHeight)
        editText.setCompoundDrawables(null, null, cancel, null)
    }

    //Function to check if the Clear button has been used
    fun EditText.onRightDrawableClicked(onClicked: (view: EditText) -> Unit) {
        this.setOnTouchListener { v, event ->
            var hasConsumed = false
            if (v is EditText) {
                if (event.x >= v.width - v.totalPaddingRight) {
                    if (event.action == MotionEvent.ACTION_UP) {
                        onClicked(this)
                    }
                    hasConsumed = true
                }
            }
            hasConsumed
        }
    }

    //Function to check
    fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }
        })
    }

    //Function to allow the URL Bar to clear its contents by clicking on Clear Button
    fun EditText.makeClearableEditText(
        onIsNotEmpty: (() -> Unit)?,
        onClear: (() -> Unit)?,
        clearDrawable: Drawable)
    {
        val updateRightDrawable = {
            this.setCompoundDrawables(null, null,
                if (text.isNotEmpty()) clearDrawable else null,
                null)
        }
        updateRightDrawable()

        this.afterTextChanged {
            if (it.isNotEmpty()) {
                onIsNotEmpty?.invoke()
            }
            updateRightDrawable()
        }
        this.onRightDrawableClicked {
            this.text.clear()
            this.setCompoundDrawables(null, null, null, null)
            onClear?.invoke()
            this.requestFocus()
        }
    }

    //Value to set where to position the Clear button
    private val COMPOUND_DRAWABLE_RIGHT_INDEX = 2

    //Function to add a function to the URL Bar
    fun EditText.makeClearableEditText(onIsNotEmpty: (() -> Unit)?, onCleared: (() -> Unit)?) {
        compoundDrawables[COMPOUND_DRAWABLE_RIGHT_INDEX]?.let { clearDrawable ->
            makeClearableEditText(onIsNotEmpty, onCleared, clearDrawable)
        }
    }

    //Function to get all Installed Apps on current device
    fun uploadInstalledApps(){
        val appsIntent = Intent(Intent.ACTION_MAIN, null)
        appsIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        applist = packageManager.queryIntentActivities(appsIntent, 0)
        for(info: ResolveInfo in applist)
        {
            appNames.add(info.activityInfo.loadLabel(packageManager).toString())
        }
        db3 = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("AppsInstalled")
        db3.setValue(appNames)
    }

    //Function to save the current website URL into the child user's profile
    fun bookmarkSite()
    {
        val bookmark = myUrlBar.text.toString()
        db5 = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID).child("Bookmarks")
        val bookKey = db5.push().key.toString()
        db5.child(bookKey).setValue(bookmark).addOnCompleteListener{task ->
            if(task.isSuccessful)
            {
                Toast.makeText(this@HomeActivity, "Bookmark was successfully added", Toast.LENGTH_SHORT).show()
            }
            else
            {
                Toast.makeText(this@HomeActivity, "There was a problem in adding the bookmark", Toast.LENGTH_SHORT).show()
            }
        }
    }
}