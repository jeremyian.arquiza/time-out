package com.timeout.time_out_v1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import com.google.firebase.database.DatabaseReference

class History : AppCompatActivity() {

    private lateinit var db : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)


        //UI Element Declarations
        val lv = findViewById<ListView>(R.id.listview)

        //Grab History from Main Activity and Display using Adapter
        var histList: java.util.ArrayList<String> = intent.getSerializableExtra("key") as ArrayList<String>
        val arrayAdapter: ArrayAdapter<String> = ArrayAdapter(
            this, R.layout.textview_layout, histList
        )
        lv.adapter = arrayAdapter
    }
}