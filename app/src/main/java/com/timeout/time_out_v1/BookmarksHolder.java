package com.timeout.time_out_v1;

import java.util.ArrayList;

public class BookmarksHolder {

    public static ArrayList<String> bookmarkKeys;
    public static ArrayList<String> bookmarkValues;
    public static String chosenKey;
    public static String chosenValue;

    public static ArrayList<String> resetKeys(){
        ArrayList<String> temp2 = bookmarkKeys;
        bookmarkKeys = new ArrayList<String>();
        return temp2;
    }

    public static ArrayList<String> resetValues(){
        ArrayList<String> temp3 = bookmarkValues;
        bookmarkValues = new ArrayList<String>();
        return temp3;
    }

    public static String resetChosenKey()
    {
        String temp = chosenKey;
        chosenKey = "";
        return temp;
    }

    public static String resetChosenValue()
    {
        String temp1 = chosenValue;
        chosenValue = "";
        return temp1;
    }

}
