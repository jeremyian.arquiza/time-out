package com.timeout.time_out_v1.Receivers;

import static android.content.Context.MODE_PRIVATE;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class BlockingReceiver extends BroadcastReceiver {

    public ArrayList<String> packlist = new ArrayList<String>();
    private FirebaseAuth fireAuth;
    private Context mcontext;
    public static int totalBlocks = 0;
    private DatabaseReference mDb, db2;
    private Calendar calendar, calendar2, calendar3;

    public BlockingReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //Function call to get saved list of blocked apps
        loadData(context);
        fireAuth = FirebaseAuth.getInstance();
        String currentUserID = fireAuth.getCurrentUser().getUid();

        //Bundle is responsible for obtaining the Start time and Stop time
        //which was saved in the Home Activity
        Bundle extrasAgain = intent.getExtras();
        String stringTimeAMPM = extrasAgain.getString("stop", "NONE FOUND");
        String packCount = extrasAgain.getString("pack", "0");
        int count = Integer.valueOf(packCount);
        long stopTime = Long.parseLong(stringTimeAMPM);
        Log.d("StopTime", String.valueOf(stopTime));
        Log.d("packCount", packCount);

        mcontext = context;
        Log.d("Saved", "onReceive called");

        //Function so that the retrieval of blocked apps will only be executed once
        //Exception if the parent decides to update the list of blocked apps while
        //the blocking state is still executing
        if(count == 0)
        {
            //BlockingList.getInstance().resetArray();
            mDb = FirebaseDatabase.getInstance().getReference().child("ChildUsers").child(currentUserID).child("Apps");
            mDb.keepSynced(true);
            mDb.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if(snapshot.exists())
                    {
                        packlist = new ArrayList<String>();
                        packlist.add("chrome");
                        packlist.add("settings");
                        packlist.add("browser");
                        for (DataSnapshot child : snapshot.getChildren())
                        {
                            String app = child.getValue(String.class);
                            assert app != null;
                            packlist.add(app);
                            //BlockingList.getInstance().getArray().add(app);
                        }
                        Toast.makeText(context, "Study Time has started.", Toast.LENGTH_SHORT).show();
                        saveAppList(context);
                    }
                    else
                    {
                        Toast.makeText(context, "There are no applications to block", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.d("BlockedApps", "Blocked Apps Error");
                }
            });
        }

        //For loop function to detect if the current application is inside the
        //list of blocked apps
        for (String apps: packlist)//(String apps: BlockingList.getInstance().getArray())
        {
            if(getCurrentApp().contains(apps))
            {
                showHomeScreen();
                Toast.makeText(context, "Application is blocked", Toast.LENGTH_SHORT).show();
            }
        }


        //If the Stop time taken from the database is equal to 1, the Stop time is then
        //converted to 4 PM and the blocking process continues until 4 PM
        if(stopTime == 1)
        {
            calendar3 = Calendar.getInstance();
            calendar3.set(Calendar.HOUR_OF_DAY, 16);
            calendar3.set(Calendar.MINUTE, 0);
            calendar3.set(Calendar.SECOND, 0);
            String stopTimeString = String.valueOf(calendar3.getTimeInMillis());
            Bundle bundle = new Bundle();
            bundle.putString("stop", stopTimeString);
            bundle.putString("pack", "1");
            Intent startIntent = new Intent(context, BlockingReceiver.class);
            startIntent.putExtras(bundle);
            final PendingIntent startPendingIntent = PendingIntent.getBroadcast(context, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            final AlarmManager startAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            startAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), startPendingIntent);
        }
        else
        {
            //If the blocking time has ended, a new alarm is set for the next day 8 AM
            if(System.currentTimeMillis() >= stopTime)
            {
                Toast.makeText(context, "Study Time has ended!", Toast.LENGTH_SHORT).show();

                calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 8);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                calendar2 = Calendar.getInstance();
                calendar2.add(Calendar.DAY_OF_MONTH, 1);
                calendar2.set(Calendar.HOUR_OF_DAY, 16);
                calendar2.set(Calendar.MINUTE, 0);
                calendar2.set(Calendar.SECOND, 0);

                Bundle lastExtras = new Bundle();
                String stringTest = String.valueOf(calendar2.getTimeInMillis());
                lastExtras.putString("stop", stringTest);
                lastExtras.putString("pack", "0");
                Intent intent2 = new Intent(context, BlockingReceiver.class);
                intent2.putExtras(lastExtras);
                final PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, 100, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                final AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager2.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent2);
                //Toast.makeText(context, "New Alarm Created", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Bundle lastExtras = new Bundle();
                lastExtras.putString("stop", stringTimeAMPM);
                lastExtras.putString("pack", "1");
                Intent intent1 = new Intent(context, BlockingReceiver.class);
                intent1.putExtras(lastExtras);
                final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 100, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, pendingIntent);
            }
        }
    }

    //Function to get the current app being used. This function makes use of the
    //UsageStats API to collect the necessary data
    public String getCurrentApp() {
        String currentApp = null;
        UsageStatsManager usm = (UsageStatsManager)mcontext.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();
        List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,  time - 10000*10000, time);
        if (appList != null && appList.size() == 0) {
            Log.d("Executed app", "######### NO APP FOUND ##########" );
        }
        if (appList != null && appList.size() > 0) {
            SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
            for (UsageStats usageStats : appList) {
                mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
            }
            if (mySortedMap != null && !mySortedMap.isEmpty()) {
                currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName().toString();
            }
        }
        return currentApp;
    }

    //Function to "block" the application when it is detected by the For Loop
    public boolean showHomeScreen(){
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(startMain);
        return true;
    }

    //Function to reset the list of blocked apps <unused>
    public ArrayList<String> resetApps()
    {
        ArrayList<String> temp = packlist;
        packlist = new ArrayList<String>();
        return temp;
    }

    //Function to save the list of blocked apps and prevent it from being erased
    //when the app is closed
    private void saveAppList(Context needContext)
    {
        SharedPreferences sharedPreferences = needContext.getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(packlist);
        editor.putString("task list", json);
        editor.apply();
    }

    //Function to load the list of blocked apps and prevent it from being reset
    //when the app is reopened
    private void loadData(Context needContext2)
    {
        SharedPreferences sharedPreferences = needContext2.getSharedPreferences("shared preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("task list", null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        packlist = gson.fromJson(json, type);

        if(packlist == null)
        {
            packlist = new ArrayList<String>();
        }
    }
}