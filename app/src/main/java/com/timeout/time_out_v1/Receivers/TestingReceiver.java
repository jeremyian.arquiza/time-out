package com.timeout.time_out_v1.Receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class TestingReceiver extends BroadcastReceiver {

    private FirebaseAuth fireAuth;
    DatabaseReference db2;
    private Calendar calendar;

    @Override
    public void onReceive(Context context, Intent intent) {
        fireAuth = FirebaseAuth.getInstance();
        String currentUserID = fireAuth.getCurrentUser().getUid();
        Bundle extras = intent.getExtras();
        String actionType = extras.getString("type", "NONE FOUND");

        if(actionType.contentEquals("Default"))
        {
            Toast.makeText(context, "Type = Default", Toast.LENGTH_SHORT).show();
            calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 16);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            String stopTimeString = String.valueOf(calendar.getTimeInMillis());
            Bundle bundle = new Bundle();
            bundle.putString("stop", stopTimeString);
            Intent startIntent = new Intent(context, BlockingReceiver.class);
            startIntent.putExtras(bundle);
            final PendingIntent startPendingIntent = PendingIntent.getBroadcast(context, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            final AlarmManager startAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            startAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, startPendingIntent);
        }
        else if(actionType.contentEquals("Normal"))
        {
            Toast.makeText(context, "Type = Normal", Toast.LENGTH_SHORT).show();
            db2 = FirebaseDatabase.getInstance().getReference().child("ChildUsers").child(currentUserID);
            db2.keepSynced(false);
            db2.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String stopTimeRec = snapshot.child("stopTime").getValue().toString();
                    Toast.makeText(context, "STOP TIME SUCCESS" + " " + stopTimeRec, Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putString("stop", stopTimeRec);
                    Intent startIntent = new Intent(context, BlockingReceiver.class);
                    startIntent.putExtras(bundle);
                    final PendingIntent startPendingIntent = PendingIntent.getBroadcast(context, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    final AlarmManager startAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    startAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, startPendingIntent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        else if(actionType.contentEquals("Reset"))
        {
            Toast.makeText(context, "Type = Reset", Toast.LENGTH_SHORT).show();
            calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 16);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            String stopTimeString = String.valueOf(calendar.getTimeInMillis());
            Bundle bundle = new Bundle();
            bundle.putString("stop", stopTimeString);
            Intent startIntent = new Intent(context, BlockingReceiver.class);
            startIntent.putExtras(bundle);
            final PendingIntent startPendingIntent = PendingIntent.getBroadcast(context, 100, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            final AlarmManager startAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            startAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, startPendingIntent);
        }
        else if(actionType.contentEquals("NONE FOUND"))
        {
            Toast.makeText(context, "Error with Start Time", Toast.LENGTH_SHORT).show();
        }
    }
}
