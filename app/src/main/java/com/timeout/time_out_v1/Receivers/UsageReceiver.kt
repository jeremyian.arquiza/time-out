package com.timeout.time_out_v1.Receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors
import kotlin.collections.LinkedHashSet

class UsageReceiver : BroadcastReceiver() {

    private val mcontext: Context? = null
    var applist = arrayListOf<String>()
    var appname = arrayListOf<String>()
    var lastused = arrayListOf<String>()
    var totalused = arrayListOf<String>()
    var outerMap = HashMap<String, Map<String, String>>()
    var innerMap = HashMap<String, String> ()
    var separatorSet = LinkedHashSet<String>()
    private lateinit var db : DatabaseReference
    private lateinit var calendar : Calendar
    private lateinit var fireAuth: FirebaseAuth


    override fun onReceive(context: Context?, intent: Intent?) {

        fireAuth = FirebaseAuth.getInstance()
        val currentUserID = fireAuth.currentUser?.uid
        applist.add("providers")
        applist.add("services")
        applist.add("telephony")
        applist.add("googlequicksearchbox")
        applist.add("calendar")
        applist.add("media")
        applist.add("downloads")
        applist.add("defcontainer")
        applist.add("carrierconfig")
        applist.add("launcher3")
        applist.add("setupwizard")
        applist.add("printspooler")
        applist.add("music")
        applist.add("sdksetup")
        applist.add("cellbroadcastreceiver")
        applist.add("keychain")
        applist.add("gms")
        applist.add("gsf")
        applist.add("latin")
        applist.add("settings")
        applist.add("phone")
        applist.add("blockednumber")
        applist.add("userdictionary")
        applist.add("systemui")
        applist.add("contacts")

        var calendar: Calendar
        val alarmManager = context!!.getSystemService(ALARM_SERVICE) as AlarmManager

        //myButtonAgain = findViewById(R.id.submitBtn)

        /*myButtonAgain.setOnClickListener{
            uploadTest()
        }
        */
        showUsageStats(context)
        uploadTest(context)

        calendar = Calendar.getInstance()
        calendar.get(Calendar.HOUR_OF_DAY)
        calendar.add(Calendar.HOUR_OF_DAY, 5)
        calendar.get(Calendar.MINUTE)
        calendar.get(Calendar.SECOND)

        val usageIntent = Intent(context, com.timeout.time_out_v1.Receivers.UsageReceiver::class.java)
        val usagePendingIntent = PendingIntent.getBroadcast(context, 69, usageIntent, 0)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, usagePendingIntent)
    }

    private fun showUsageStats(context1 : Context?)
    {
        var usageStatsManager: UsageStatsManager = context1!!.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        var queryUsageStats : List<UsageStats> = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, System.currentTimeMillis() - 1000*3600*24, System.currentTimeMillis())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            queryUsageStats = queryUsageStats.stream().filter { app -> app.getTotalTimeInForeground() > 0 }
                .collect(Collectors.toList())
        }
        Collections.reverse(queryUsageStats)
        var stats_data : String = ""
        loop@ for (i in 0..queryUsageStats.size-1)
        {
            var testnum = 0
            check@ for (app in applist)
            {
                if(queryUsageStats.get(i).packageName.contains(app))
                {
                    testnum += 1
                }
            }
            if(testnum > 0)
            {
                continue@loop
            }
            else
            {

                appname.add(queryUsageStats[i].packageName)
                lastused.add(convertTime(queryUsageStats[i].lastTimeUsed))
                totalused.add(totalConvert(queryUsageStats.get(i).totalTimeInForeground))
            }
        }
    }

    private fun convertTime (lastTimeUsed: Long): String
    {
        var date: Date = Date(lastTimeUsed)
        var format: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH)
        return format.format(date)
    }

    private fun convertTime2 (lastTimeUsed: Long) : String
    {
        var date: Date = Date(lastTimeUsed)
        var format: SimpleDateFormat = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)
        return format.format(date)
    }

    private fun uploadTest(context2 : Context?) {

        fireAuth = FirebaseAuth.getInstance()
        val currentUserID = fireAuth.currentUser?.uid

        /*
        //Clean app names
        separatorSet.addAll(appname)
        appname.clear()
        appname.addAll(separatorSet)
        separatorSet.clear()
        //Clean last used
        separatorSet.addAll(lastused)
        lastused.clear()
        lastused.addAll(separatorSet)
        separatorSet.clear()
        //Clean total used
        separatorSet.addAll(totalused)
        totalused.clear()
        totalused.addAll(separatorSet)
        separatorSet.clear()

         */


        for(j in 0..appname.size-1)
        {
            innerMap.put("App", appname[j])
            innerMap.put("Last", lastused[j])
            innerMap.put("Total", totalused[j])
            outerMap.put("Application$j", innerMap)

            db = FirebaseDatabase.getInstance().reference.child("ChildUsers").child(currentUserID.toString()).child("AppUsage")
            db.setValue(outerMap)/*.addOnCompleteListener{ task ->
                if(task.isSuccessful)
                {
                    Toast.makeText(this@UsageStatsTest, "Usage Uploaded!", Toast.LENGTH_LONG).show()
                }
                else
                {
                    Toast.makeText(this@UsageStatsTest, task.exception!!.message.toString(), Toast.LENGTH_SHORT).show()
                }
            }
            */
            innerMap = HashMap<String, String> ()
        }

    }

    private fun totalConvert(millisTime: Long) : String{
        var tempMillisTime = millisTime
        if(millisTime < 0)
        {
            throw IllegalArgumentException("Duration must be greater than 0")
        }

        var hourTime = TimeUnit.MILLISECONDS.toHours(tempMillisTime)
        tempMillisTime -= TimeUnit.HOURS.toMillis(hourTime)
        var minuteTime = TimeUnit.MILLISECONDS.toMinutes(tempMillisTime)
        tempMillisTime -= TimeUnit.MINUTES.toMillis(minuteTime)
        var secondTime = TimeUnit.MILLISECONDS.toSeconds(tempMillisTime)

        return ("" + hourTime + "h " + minuteTime + "m " + secondTime + "s")
    }

}