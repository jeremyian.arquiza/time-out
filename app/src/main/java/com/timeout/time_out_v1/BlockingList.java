package com.timeout.time_out_v1;

import java.util.ArrayList;

public class BlockingList {

    private static BlockingList mInstance;
    private ArrayList<String> list = null;

    public static BlockingList getInstance() {
        if(mInstance == null)
            mInstance = new BlockingList();

        return mInstance;
    }

    private BlockingList() {
        list = new ArrayList<String>();
    }
    // retrieve array from anywhere
    public ArrayList<String> getArray() {
        return this.list;
    }
    //Add element to array
    public void addToArray(String value) {
        list.add(value);
    }

    public void resetArray(){
        list.clear();
    }

}
