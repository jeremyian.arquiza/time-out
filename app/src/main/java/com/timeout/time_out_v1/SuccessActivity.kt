package com.timeout.time_out_v1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SuccessActivity : AppCompatActivity() {

    private lateinit var fireAuth: FirebaseAuth
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var successChildBtn : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)

        fireAuth = FirebaseAuth.getInstance()
        firebaseUser = fireAuth.currentUser!!
        successChildBtn = findViewById(R.id.success_delete_button)

        successChildBtn.setOnClickListener{
            firebaseUser.delete().addOnCompleteListener{
                task ->
                if(task.isSuccessful)
                {
                    fireAuth.signOut()
                    val intent = Intent(this@SuccessActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    Toast.makeText(this@SuccessActivity, "Profile successfully deleted. Thank you for using Time-Out.", Toast.LENGTH_SHORT).show()
                }
                else
                {
                    Toast.makeText(this@SuccessActivity, "There was an error", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }
}